var floatingPoints = [];
floatingPointCycleDuration = 50;

var metrics = {
  changeScore : function(change) {
    hud.scoreValue += change;
    if (hud.scoreValue < 0) {
      hud.scoreValue = 0;
    };
  },
  addNewFloatingPoint : function(x, y, points, action) {
    symbol = "+"
    color = "black"
    if (action == "lose") {
      symbol = "-"
      color = "red"
    };
    newPoint = new component("20px", "Consolas", color, x, y, "text");
    newPoint.text = symbol + points;
    newPoint.cycleNumber = 0;
    floatingPoints.push(newPoint);
  },
  updateFloatingPoints : function() {
    for (i = 0; i < floatingPoints.length; i += 1) {
      floatingPoints[i].cycleNumber += 1;
      floatingPoints[i].y -= 1;
      floatingPoints[i].update();
      if (floatingPoints[i].cycleNumber > floatingPointCycleDuration) {
        floatingPoints.splice(i, 1);
      }
    };
  },
};
