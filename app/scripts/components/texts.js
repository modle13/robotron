var texts = {
  score : new component("30px", "Consolas", "black", canvasWidth/10, 40, "text"),
  timer : new component("30px", "Consolas", "black", canvasWidth/3, 40, "text"),
  livesDisplay : new component("30px", "Consolas", "black", canvasWidth/2, 40, "text"),
  level : new component("30px", "Consolas", "black", canvasWidth/3*2, 40, "text"),
  pausedMessage : new component("50px", "Consolas", "Black", canvasWidth/4, canvasHeight/4, "text"),
  died : new component("50px", "Consolas", "Black", canvasWidth/4, canvasHeight/4, "text"),
  levelOver : new component("40px", "Consolas", "black", canvasWidth/4, canvasHeight/5*2, "text"),
  init : function() {
    this.levelOver.text = "";
  },
}

texts.init();
