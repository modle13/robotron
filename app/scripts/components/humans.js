var humans = {
  humans : [],
  interval : 50,
  speed : 1,
  pointValue : 5,
  manage : function() {
    if (gameArea.frameNo == 1) {
      this.spawn(hud.currentLevel + 3);
    }
    this.update();
  },
  spawn : function(amount) {
    while (this.humans.length < amount) {
      x = Math.floor(Math.random() * (canvasWidth + 1));
      y = Math.floor(Math.random() * (canvasHeight + 1));
      this.humans.push(new component(30, 30, "Plum", x, y, 0, 0));
    }
  },
  update : function() {
    // humans move slowly and randomly
    for (i = 0; i < this.humans.length; i += 1) {
      directionX = Math.floor(Math.random() * 3 - 1);
      directionY = Math.floor(Math.random() * 3 - 1);
      this.humans[i].x += this.speed * directionX;
      this.humans[i].y += this.speed * directionY;
      this.checkBoundaries(this.humans[i]);
    }
    for (i = 0; i < this.humans.length; i += 1) {
      this.humans[i].update();
    }
  },
  checkBoundaries : function(human) {
    if (human.x < 0) {
      human.x = 0;
    }
    if (human.x > canvasWidth) {
      human.x = canvasWidth - human.width;
    }
    if (human.y < 0) {
      human.y = 0;
    }
    if (human.y > canvasHeight) {
      human.y = canvasHeight - human.height;
    }
  },
  clear : function() {
    this.humans = [];
  },
};
