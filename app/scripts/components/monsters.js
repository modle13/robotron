var monsters = {
  monsters : [],
  interval : 10,
  baseSpeed : 2,
  max : 30,
  pointValue : 1,
  manage : function() {
    if (gameArea.frameNo == 1 || everyinterval(this.interval)) {
      this.spawn();
    }
    this.update();
  },
  spawn : function() {
    if (!gamePiece || !gamePiece.gamePiece.x || !gamePiece.gamePiece || !canvasHeight || !canvasWidth) {
      console.log("something's wrong at monster spawn, Jim");
    }
    while (this.monsters.length < this.max) {
      x = Math.floor(Math.random()*(canvasWidth-0+1));
      y = Math.floor(Math.random()*(canvasHeight-0+1));
      // prevent them from spawning too close to gamePiece
      if (Math.abs(x - gamePiece.gamePiece.x) > canvasWidth/10 && Math.abs(y - gamePiece.gamePiece.y) > canvasHeight/10) {
        this.monsters.push(new component(15, 15, "blue", x, y, 0, 0));
      }
    }
  },
  update : function() {
    if (everyinterval(this.interval)) {
    // monsters move toward the game piece
      var gamePieceCenterX = gamePiece.gamePiece.x + gamePiece.gamePiece.width / 2;
      var gamePieceCenterY = gamePiece.gamePiece.y + gamePiece.gamePiece.height / 2;
      var centerX;
      var centerY;
      for (i = 0; i < this.monsters.length; i += 1) {
        centerX = this.monsters[i].x + this.monsters[i].width / 2;
        centerY = this.monsters[i].y + this.monsters[i].height / 2;
        if (centerX < gamePieceCenterX) {
          this.monsters[i].x += this.getSpeed();
        }
        if (centerX > gamePieceCenterX) {
          this.monsters[i].x += -this.getSpeed();
        }
        if (centerY < gamePieceCenterY) {
          this.monsters[i].y += this.getSpeed();
        }
        if (centerY > gamePieceCenterY) {
          this.monsters[i].y += -this.getSpeed();
        }
      }
    }
    for (i = 0; i < this.monsters.length; i += 1) {
      this.monsters[i].update();
    }
  },
  getSpeed : function() {
    return this.baseSpeed + hud.currentLevel;
  },
  clear : function() {
    this.monsters = [];
  },
};
