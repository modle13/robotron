
var gamePiece = {
  gamePieceSpeed : 5,
  gamePieceWidth : 30,
  gamePieceHeight : 30,
  init : function() {
    this.gamePieceStartX = (canvasWidth - this.gamePieceWidth) / 2;
    this.gamePieceStartY = (canvasHeight - this.gamePieceHeight) / 2;
    this.gamePiece = new component(this.gamePieceWidth, this.gamePieceHeight, "red", this.gamePieceStartX, this.gamePieceStartY, 0, 0);
  },
  manage : function() {
    this.move();
    this.update();
  },
  update : function() {
    this.gamePiece.newPos();
    this.gamePiece.update();
  },
  reset : function() {
    this.gamePiece.x = this.gamePieceStartX;
    this.gamePiece.y = this.gamePieceStartY;
  },
  move : function() {
    this.gamePiece.speedX = 0;
    this.gamePiece.speedY = 0;
    this.gamePieceRight = this.gamePiece.x + this.gamePiece.width;
    this.gamePieceLeft = this.gamePiece.x;
    this.gamePieceTop = this.gamePiece.y;
    this.gamePieceBottom = this.gamePiece.y + this.gamePiece.height;

    // wrap gamePiece to other side of screen when exiting canvas
    if (this.gamePieceLeft > canvasWidth) {
      this.gamePiece.x = 0 - this.gamePiece.width;
    }
    if (this.gamePieceRight < 0) {
      this.gamePiece.x = canvasWidth;
    }
    if (this.gamePieceTop > canvasHeight) {
      this.gamePiece.y = 0 - this.gamePiece.height;
    }
    if (this.gamePieceBottom < 0) {
      this.gamePiece.y = canvasHeight;
    }

    // move game piece
    // left
    if (gameArea.keys && gameArea.keys[65]) {
      this.gamePiece.speedX = -this.gamePieceSpeed;
    }
    // right
    if (gameArea.keys && gameArea.keys[68]) {
      this.gamePiece.speedX = this.gamePieceSpeed;
    }
    // up
    if (gameArea.keys && gameArea.keys[87]) {
      this.gamePiece.speedY = -this.gamePieceSpeed;
    }
    // down
    if (gameArea.keys && gameArea.keys[83]) {
      this.gamePiece.speedY = this.gamePieceSpeed;
    }
  },
}

gamePiece.init();
