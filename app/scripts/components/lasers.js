var lasers = {
  lasers : [],
  speed : 10,
  sideLength : 5,
  interval : 10,
  manage : function() {
    this.make();
    this.update();
    this.clearOutsideCanvas();
  },
  make : function() {
    if (!this.lasers || this.lasers.length > 5 || !gameArea.keys || !everyinterval(this.interval)) {
      return;
    }
    var speedX = 0;
    var speedY = 0;
    // left up
    if (gameArea.keys[37] && gameArea.keys[38]) {
      speedX = -1 * this.speed;
      speedY = -1 * this.speed;
    // left down
    } else if (gameArea.keys[37] && gameArea.keys[40]) {
      speedX = -1 * this.speed;
      speedY = 1 * this.speed;
    // right up
    } else if (gameArea.keys[39] && gameArea.keys[38]) {
      speedX = 1 * this.speed;
      speedY = -1 * this.speed;
    // right down
    } else if (gameArea.keys[39] && gameArea.keys[40]) {
      speedX = 1 * this.speed;
      speedY = 1 * this.speed;
    // left
    } else if (gameArea.keys[37]) {
      speedX = -1 * this.speed;
      speedY = 0 * this.speed;
    // right
    } else if (gameArea.keys[39]) {
      speedX = 1 * this.speed;
      speedY = 0 * this.speed;
    // up
    } else if (gameArea.keys[38]) {
      speedX = 0 * this.speed;
      speedY = -1 * this.speed;
    // down
    } else if (gameArea.keys[40]) {
      speedX = 0 * this.speed;
      speedY = 1 * this.speed;
    }
    if (speedX != 0 || speedY != 0) {
      this.lasers.push(new component(this.sideLength, this.sideLength, "green", gamePiece.gamePiece.x + gamePiece.gamePiece.width / 2, gamePiece.gamePiece.y + gamePiece.gamePiece.height / 2, "laser", speedX, speedY));
    }
  },
  update : function() {
    for (i = 0; i < this.lasers.length; i += 1) {
      this.lasers[i].x += this.lasers[i].speedX;
      this.lasers[i].y += this.lasers[i].speedY;
      this.lasers[i].update();
    }
  },
  clearOutsideCanvas : function() {
    if (this.lasers == false) { return; }
    for (i = 0; i < this.lasers.length; i += 1) {
      if (this.lasers[i].x > canvasWidth || this.lasers[i].x < 0 || this.lasers[i].y > canvasHeight || this.lasers[i].y < 0) {
        // laser
        this.lasers.splice(i, 1);
      }
    }
  },
  clear : function() {
    this.lasers = [];
  },
};
