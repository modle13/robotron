var hud = {
  scoreValue : 0,
  defaultLives : 3,
  currentLevel : 1,
  init : function() {
    this.lives = this.defaultLives;
  },
  updateHud : function() {
    this.updateScore();
    this.updateTimer();
    this.updateLives();
    this.updateLevel();
  },
  updateScore : function() {
    texts.score.text = "Score: " + this.scoreValue;
    texts.score.update();
  },
  updateTimer : function() {
    texts.timer.text = getTime();
    texts.timer.update();
  },
  updateLives : function() {
    texts.livesDisplay.text = "Lives: " + this.lives;
    texts.livesDisplay.update();
  },
  updateLevel : function() {
    texts.level.text = "Level: " + this.currentLevel;
    texts.level.update();
  },
};

hud.init();
