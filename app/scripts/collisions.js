var collisions = {
  check() {
    this.checkGamePieceVsHumans();
    this.checkGamePieceVsMonsters();
    this.checkLaserVsMonsters();
    this.checkMonsterVsHumans();
  },
  checkMonsterVsHumans() {
    for (i = 0; i < monsters.monsters.length; i += 1) {
      for (j = 0; j < humans.humans.length; j += 1) {
        if (humans.humans[j].crashWith(monsters.monsters[i])) {
          // update scoreValue
          metrics.addNewFloatingPoint(humans.humans[j].getMiddleX(), humans.humans[j].getMiddleY(), 2, "lose");
          metrics.changeScore(-2);
          // remove monster
          humans.humans.splice(j, 1);
        }
      }
    }
  },
  checkGamePieceVsHumans() {
    for (i = 0; i < humans.humans.length; i += 1) {
      if (gamePiece.gamePiece.crashWith(humans.humans[i])) {
        metrics.addNewFloatingPoint(humans.humans[i].getMiddleX(), humans.humans[i].getMiddleY(), humans.pointValue, "gain");
        metrics.changeScore(humans.pointValue);
        humans.humans.splice(i, 1);
      }
    }
  },
  checkGamePieceVsMonsters() {
    for (i = 0; i < monsters.monsters.length; i += 1) {
      if (gamePiece.gamePiece.crashWith(monsters.monsters[i])) {
        texts.died.text = "You died.";
        texts.died.update();
        hud.lives -= 1;
        if (hud.lives > 0) {
          return;
        }
        gameArea.stop();
        gameOver = new component("100px", "Consolas", "pink", canvasWidth/4, canvasHeight/2, "text");
        gameOver.text = "Game Over";
        gameOver.update();
      }
    }
  },
  checkLaserVsMonsters() {
    for (i = 0; i < lasers.lasers.length; i += 1) {
      for (j = 0; j < monsters.monsters.length; j += 1) {
        if (lasers.lasers[i].crashWith(monsters.monsters[j])) {
          // add floating point
          metrics.addNewFloatingPoint(monsters.monsters[j].getMiddleX(), monsters.monsters[j].getMiddleY(), monsters.pointValue, "gain");
          // remove monster and set laser removal to pending
          monsters.monsters.splice(j, 1);
          lasers.lasers[i].remove = true;
          // update scoreValue
          metrics.changeScore(monsters.pointValue);
        }
      }
      if (lasers.lasers[i].remove) {
        lasers.lasers.splice(i, 1);
      }
    }
  },
};
