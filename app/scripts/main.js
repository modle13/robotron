var paused = true;

var game = {
  start : function() {
    if (isMobile()) {
      return;
    }
    gameArea.start();
  },
  reset : function() {
    monsters.clear();
    lasers.clear();
    humans.clear();
    gameArea.stop();
    scoreValue = 0;
    lives = defaultLives;
    this.startGame();
  },
  clearGameAreaAndBumpFrame : function() {
    gameArea.clear();
    gameArea.frameNo += 1;
  },
  checkLevelEndConditions : function() {
    if (humans.humans.length == 0) {
      texts.levelOver.text = "Level clear! Loading next level...";
    }
    if (getTime() < 1) {
      texts.levelOver.text = "Time's up! Loading next level...";
    }
    texts.levelOver.update();
  },
  loadNextLevel : function() {
    lasers.clear();
    monsters.clear();
    humans.clear();
    floatingPoints = []
    gameArea.frameNo = 0;
    humans.spawn();
    gamePiece.reset();
    hud.currentLevel += 1;
  },
  managePause : function() {
    texts.pausedMessage.text = "Paused: Spacebar to Continue";
    if (gameArea.frameNo == 0) {
      texts.pausedMessage.text = "Press Spacebar to Start";
    }
    texts.pausedMessage.update();
    return;
  },
  manageLevel : function() {
    if (texts.levelOver.text) {
      wait(2000);
      this.loadNextLevel();
      texts.levelOver.text = "";
    }
  },
  cleanUp : function() {
    monsters.clear();
    lasers.clear();
    gamePiece.reset();
    texts.died.text = "";
  },
  manageDeath : function() {
    if (texts.died.text) {
      wait(2000);
      this.cleanUp();
    }
  },
};

// this gets executed every interval
function updateGameArea() {
  if (paused) {
    game.managePause();
    return;
  }
  game.manageLevel();
  game.manageDeath();
  game.clearGameAreaAndBumpFrame();
  collisions.check();
  humans.manage();
  monsters.manage();
  lasers.manage();
  gamePiece.manage();
  hud.updateHud();
  metrics.updateFloatingPoints();
  game.checkLevelEndConditions();
};
